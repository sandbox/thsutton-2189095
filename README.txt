Commerce Order User UI
======================

This module provides an interface for *users* to interact with some aspects of
their orders. This is useful when the Commerce Order entity has custom fields
which the user must edit before or while adding products to their cart.

Dependencies
------------

*Commerce Order User UI* has the following dependencies:

- [Entity API](https://drupal.org/project/entity)
- [Drupal Commerce](https://drupal.org/project/commerce) and Commerce Cart
- [Field Group](https://drupal.org/project/field_group)

Install
-------

Download *Commerce Order User UI* and unpack it in `sites/all/modules/` (or
another module location).

Navigate to `admin/modules` and enable the module as normal.

Configuration
-------------

Configuring *Commerce Order User UI* to meet your requirements is fairly simple:

1. Add one or more field groups to the Commerce Order entity and place the
   fields your users can edit in those groups.

2. Configure a view mode for the Commerce Order entity to display the fields
   and other details you need. You may want to use a module like [Entity view
   modes](https://drupal.org/project/entity_view_mode) to define a new view mode
   specifically for this use case.

3. Visit `admin/structure/block` and *configure* the "Commerce cart details"
   block. Select the field groups and view mode you configured above and assign
   the block to a region as required.

4. Visit `admin/people/permissions` and grant the "Access commerce order user
   UI" permissions to all roles which should be able to access the block.

5. Test it!
